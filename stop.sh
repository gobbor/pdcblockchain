#!/bin/sh
if [ -e ~/.pdc/pdc.pid ]; then
    PID=`cat ~/.pdc/pdc.pid`
    ps -p $PID > /dev/null
    STATUS=$?
    echo "stopping"
    while [ $STATUS -eq 0 ]; do
        kill `cat ~/.pdc/pdc.pid` > /dev/null
        sleep 5
        ps -p $PID > /dev/null
        STATUS=$?
    done
    rm -f ~/.pdc/pdc.pid
    echo "Pdc server stopped"
fi

