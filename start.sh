#!/bin/sh
if [ -e ~/.pdc/pdc.pid ]; then
    PID=`cat ~/.pdc/pdc.pid`
    ps -p $PID > /dev/null
    STATUS=$?
    if [ $STATUS -eq 0 ]; then
        echo "Pdc server already running"
        exit 1
    fi
fi
mkdir -p ~/.pdc/
DIR=`dirname "$0"`
cd "${DIR}"
if [ -x jre/bin/java ]; then
    JAVA=./jre/bin/java
else
    JAVA=java
fi
nohup ${JAVA} -cp classes:lib/*:conf:addons/classes:addons/lib/* -Dwin.runtime.mode=desktop pdc.Pdc > /dev/null 2>&1 &
echo $! > ~/.pdc/pdc.pid
cd - > /dev/null
