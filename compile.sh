#!/bin/sh
CP="lib/*:classes"
SP=src/java/

/bin/rm -f pdc.jar
/bin/rm -f winservice.jar
/bin/rm -rf classes
/bin/mkdir -p classes/
/bin/rm -rf addons/classes
/bin/mkdir -p addons/classes/

echo "compiling pdc core..."
find src/java/pdc/ -name "*.java" > sources.tmp
javac -encoding utf8 -sourcepath "${SP}" -classpath "${CP}" -d classes/ @sources.tmp || exit 1
echo "pdc core class files compiled successfully"

rm -f sources.tmp

echo "compilation done"
