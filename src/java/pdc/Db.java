/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package pdc;

import pdc.db.BasicDb;
import pdc.db.TransactionalDb;

public final class Db {

    public static final String PREFIX = Constants.isTestnet ? "pdc.testDb" : "pdc.db";
    public static final TransactionalDb db = new TransactionalDb(new BasicDb.DbProperties()
            .maxCacheSize(Pdc.getIntProperty("pdc.dbCacheKB"))
            .dbUrl(Pdc.getStringProperty(PREFIX + "Url"))
            .dbType(Pdc.getStringProperty(PREFIX + "Type"))
            .dbDir(Pdc.getStringProperty(PREFIX + "Dir"))
            .dbParams(Pdc.getStringProperty(PREFIX + "Params"))
            .dbUsername(Pdc.getStringProperty(PREFIX + "Username"))
            .dbPassword(Pdc.getStringProperty(PREFIX + "Password", null, true))
            .maxConnections(Pdc.getIntProperty("pdc.maxDbConnections"))
            .loginTimeout(Pdc.getIntProperty("pdc.dbLoginTimeout"))
            .defaultLockTimeout(Pdc.getIntProperty("pdc.dbDefaultLockTimeout") * 1000)
            .maxMemoryRows(Pdc.getIntProperty("pdc.dbMaxMemoryRows"))
    );

    static void init() {
        db.init(new PdcDbVersion());
    }

    static void shutdown() {
        db.shutdown();
    }

    private Db() {} // never

}
