
package pdc;

import pdc.Constants;
import pdc.util.Logger;

public final class Coinbase {
  public static long computeCoinbase(long height) {
    long remainingCoinbase = Math.max(Constants.COINBASE_MAX - (height * Constants.COINBASE_REWARD), 0L);
    long payout = (remainingCoinbase > Constants.COINBASE_REWARD) ? Constants.COINBASE_REWARD : remainingCoinbase;
    return payout * Constants.ONE_PDC;
  }
}
