#!/bin/sh
java -cp classes pdc.tools.ManifestGenerator
/bin/rm -f pdc.jar
jar cfm pdc.jar resource/pdc.manifest.mf -C classes . || exit 1
/bin/rm -f winservice.jar
jar cfm winservice.jar resource/winservice.manifest.mf -C classes . || exit 1

echo "jar files generated successfully"
